var config    = require('../config/config.js');
var pg        = require('pg');
var conString = "postgres://"+config.db.user+":"+config.db.password+"@"+config.db.host+"/"+config.db.database;
var client    = new pg.Client(conString);

client.connect(function(err) {
    if(err) throw err;

    console.log("DB connected");
});

module.exports = client;
