var express = require('express');
var router  = express.Router();

router.get('/', function (req, res) {
  res.render('index.twig');
});

router.get('/home', function (req, res) {
  res.render('home.twig');
});

module.exports = router;
