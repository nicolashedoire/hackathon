module.exports.http       = require('http');
module.exports.https      = require('https');
module.exports.fs         = require('fs');
module.exports.io         = require('socket.io');
module.exports.path       = require('path');
module.exports.request    = require('request');
module.exports.favicon    = require('serve-favicon');
module.exports.logger     = require('morgan');
module.exports.bodyParser = require('body-parser');
module.exports.twig       = require('twig');
module.exports.express    = require('express');

// Evenements
module.exports.google     = require('./events/google.js');
module.exports.activity   = require('./events/activity.js');
