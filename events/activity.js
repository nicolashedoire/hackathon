module.exports = function(socket, app) {
    // Retourne la liste des utilisateurs qui s'entrainent
    socket.on('getTrainingUsers', function(data){
        socket.emit('getTrainingUsers', app.locals.trainingUsers);
    })

    // Evenement appelé toutes les x secondes pour mettre à jour les données
    // courantes de l'utilisateur
    socket.on('refreshActivityStatus', function(data) {
        console.log('refreshActivityStatus');
        if(app.locals.connectedUsers.hasOwnProperty(data.kinomapId)) {
            var user = app.locals.connectedUsers[data.kinomapId];
            app.locals.trainingUsers[data.kinomapId] = {
                kinomapId: data.kinomapId,
                avatarUrl: user.info.avatar_url,
                mediaId: data.mediaId,
                mediaUrl: data.mediaUrl,
                mediaName: data.mediaName,
                kinos: data.kinos,
                elapsedTime: data.elapsedTime,
                power: data.power,
                hardwareName: data.hardwareName,
                lastUpdate: new Date(),
            };
            socket.emit('activityStatusRefreshed', app.locals.trainingUsers[data.kinomapId]);
        }
    });

    // Evenement appelé pour sauvegarder une activité sur Kinomap
    socket.on('saveActivity', function(data) {
        // Suppression de la personne dans le tableau des utilisateurs
        // qui s'entrainent
        delete app.locals.trainingUsers[data.kinomapId];

        console.log("INSERT IN DATABASE NOT YET IMPLEMENTED");
        /*db.query(
            'INSERT INTO activity (act_mem_id, act_nb_sessions, act_complete, act_training_mode) VALUES ($1, $2, $3, $4) RETURNING act_id',
            [data.kinomapid, 1, 1, 2],
            function(err, result) {
                if(err) throw err;

                if(result.rowCount === 1) {
                    var activityId = result.rows[0].act_id;
                }
            }
        );*/
    });
};
