var db     = require('../services/db.js');
var crypto = require('crypto');

module.exports = function(socket, app) {
    socket.on('googleOauth', function(data) {
        var user = {};
        var selectFields = "id, username, email, avatar_url, google_id";
        console.log('Google Oauth:' + data.wc);

        db.query('SELECT '+selectFields+' FROM users WHERE google_id = $1::text LIMIT 1', [data.Ka], function(err, result) {
            if(err) throw err;

            if(result.rows.length === 1) {
                console.log("Existing account");
                // Un compte Kinomap est déjà rattacher à cet identifiant Google
                // On va donc mettre à jour les données Google puis connecter la
                // personne
                user = result.rows[0];
                db.query('UPDATE users SET google_data = $1 WHERE id = $2', [data, user.id], function(err, result) {
                    if(err) throw err;

                    app.locals.connectedUsers[user.id] = {info:user, socket:socket};
                    socket.emit('loggedIn', user);
                    return;
                });
            } else {
                // Aucun compte attaché à l'identifiant Google
                db.query('SELECT '+selectFields+' FROM users WHERE email = $1 LIMIT 1', [data.hg], function(err, result) {
                    if(err) throw err;

                    if(result.rows.length === 1) {
                        user = result.rows[0];
                        console.log("Associating account:" + user.username);
                        // Un compte Kinomap existe déjà avec l'adresse e-mail Google
                        // On va donc associer le compte Kinomap à celui de Google
                        db.query('UPDATE users SET google_data = $1, google_id = $2 WHERE id = $3', [data, data.Ka, user.id], function(err, result) {
                            if(err) throw err;

                            app.locals.connectedUsers[user.id] = {info:user, socket:socket};
                            socket.emit('loggedIn', user);
                            return;
                        });
                    } else {
                        var tryAgain = true;
                        var count    = 1;
                        var uniqid   = (new Date().getTime()).toString(16);

                        var ENABLED  = 1;
                        var LANG     = 2;
                        var ROLES    = 'a:1:{i:0;s:9:"ROLE_USER";}';
                        var baseUsername = data.Za + data.Na;
                        var username = baseUsername;
                        var fullname = data.wc;
                        var email    = data.hg;
                        var googleId = data.Ka;
                        var password = crypto.createHash('md5').update(uniqid).digest('hex');
                        var salt     = uniqid;

                        // On recherche un username disponible par rapport au username Google
                        do {
                            db.query('SELECT * FROM users WHERE username = $1', [username], function(err, result) {
                                if(err) throw err;

                                if(result.rows.length === 0) {
                                    tryAgain = false;
                                } else {
                                    username = baseUsername + count.toString();
                                }
                            });
                        } while (true === tryAgain);

                        // On va créer un compte Kinomap avec les informations Google
                        db.query(
                            'INSERT INTO users (username, username_canonical, email, email_canonical, old_password, old_access_token, full_name, roles, lang_id, mail_not_validated, enabled, expired, credentials_expired, salt, locked, google_data, google_id, avatar_url) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18)',
                            [username, username, email, email, password, uniqid, fullname, ROLES, LANG, 0, ENABLED, 0, 0, uniqid, 0, data, googleId, data.Ph],
                            function(err, result) {
                                if(err) throw err;

                                if(result.rowCount === 1) {
                                    console.log("Creating account");
                                    db.query('SELECT '+selectFields+' FROM users WHERE google_id = $1::text LIMIT 1', [data.Ka], function(err, result) {
                                        if(err) throw err;

                                        if(result.rows.length === 1) {
                                            user = result.rows[0];
                                            app.locals.connectedUsers[user.id] = {info:user, socket:socket};
                                            socket.emit('loggedIn', user);
                                            return;
                                        }
                                    });
                                }
                            }
                        );
                    }
                });
            }
        });
    });
};
