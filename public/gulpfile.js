////////////////////////////////
// Required
////////////////////////////////

var gulp = require("gulp");
var compass = require('gulp-compass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var livereload = require('gulp-livereload');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var del = require('del');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');

/////////////////////////////////////
// Script Tasks
/////////////////////////////////////

gulp.task('compass', function(){
	return gulp.src('scss/**/*.scss')
		.pipe(compass({
			config_file : './config.rb',
			css : 'css',
			sass : 'scss',
			require : ['susy']
		}))
		.pipe(gulp.dest('css'))
		.pipe(rename({suffix : '.min'}))
		.pipe(minifyCss({compatbility : 'ie8'}))
		.pipe(gulp.dest('dist/css'));
/*		.pipe(livereload());*/
});

gulp.task('scripts', function(){
	gulp.src('js/**/*.js')
		.pipe(rename({suffix : '.min'}))
		.pipe(uglify())
		.pipe(gulp.dest('dist/js'));

});

gulp.task('default', ['scripts', 'compass'], function(){

});

gulp.task('watch', function(){
	var server = livereload.listen(35729, function(err){

	});
	gulp.watch('js/**/*.js', ['scripts'] ).on('change', function(e){
		console.log('file ' + e.path + ' modified');
	});
	gulp.watch('scss/**/*.scss', ['compass'] ).on('change', function(e){
		console.log('file ' + e.path + ' modified');
	});
});
