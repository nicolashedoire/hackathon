var R       = require('./require.js');
var express = R.express;
var app     = express();
var config  = require('./config/config.js');

// **********
// Variables globales
// **********
app.locals.connectedUsers = {}; // Tableau des utilisateurs connectés
app.locals.trainingUsers  = {}; // Tableau des utilisateurs en train de s'entrainer

// **********
// Création du serveur Nodejs
// **********
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var privateKey  = R.fs.readFileSync('/root/wildcard-kinomap-com.key', 'utf8');
var certificate = R.fs.readFileSync('/root/wildcard-kinomap-com.cer', 'utf8');
var ca = R.fs.readFileSync('/root/GeoTrustGlobalCA.cer')
var credentials = {key: privateKey, cert: certificate, ca: ca};
var server = R.https.createServer(credentials, app).listen(config.port);
var socket = require('socket.io').listen(server); // Gestion des sockets pour le temps réel
socket.set('transports', [ 'websocket' ]);

// **********
// Configurations des différentes librairies
// **********
app.use(R.bodyParser.urlencoded({ extended: true }));
app.use(R.bodyParser.json());
app.set('port', process.env.PORT || config.port);
app.use(R.express.static(R.path.join(__dirname, './public/')));
app.set('views', R.path.join(__dirname, './public/views'));
app.set('view engine', 'twig');
app.set("view options", { layout: false });
app.set('twig options', {strict_variables: false});
app.use(R.logger('dev'));

// **********
// Routing
// **********
var routes = require('./config/routes.js');
app.use('/', routes);

// allow any user to authenticate.
socket.set('authorization', function(handshake, callback)
{
    return callback(null, true);
});

socket.on('connection', function(socket) {
    console.log('Socket connected: ' + socket.id);

    // Gestion des évènements liés à Google
    var google   = new R.google(socket, app);
    var activity = new R.activity(socket, app);
});
